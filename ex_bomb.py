#!/usr/bin/env python2.7

# bomb example

import os
import sys
import time

THRESHOLD = 10

def error(message, *args):
    print >>sys.stderr, message.format(*args)
    sys.exit(1)

for prompt in ('ARE YOU SURE YOU WANT TO DO THIS?', 'DID YOU SET SET NPROCS ULIMIT?'):
    response = raw_input(prompt + ' ')
    if response.lower() not in ('yes', 'y', 'si', 'oui', 'ya', 'yeah'):
        sys.exit(0)

for threshold in range(THRESHOLD):
    try:
        pid = os.fork()
        time.sleep(4)       # Give us a chance to kill it
    except OSError as e:    # Error
        error('Unable to fork: {}', e)
    except KeyboardInterrupt:
        sys.exit(0)
