#!/usr/bin/env python2.7

# fork/wait example

import os
import sys

try:
    pid = os.fork()
except OSError as e:# Error
    print >>sys.stderr, 'Unable to fork: {}'.format(e)
    sys.exit(1) 

if pid == 0:    # Child
    print 'Child pid:  {}, Parent pid: {}'.format(os.getpid(), os.getppid())
else:           # Parent
    print 'Parent pid: {}, Child pid:  {}'.format(os.getpid(), pid)
    pid, status = os.wait()
    print 'Child pid:  {}, Exit Status: {}'.format(pid, status)
