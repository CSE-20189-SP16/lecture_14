Lecture 14 - System Calls (Processes, Signals)
==============================================

This repository contains examples of using system calls related to [processes]
and [signals]:

1. `ex_fork_wait.py`: An example of forking and waiting.

2. `ex_fork_exec.py`: An example of forking, execing, and waiting.

3. `ex_system.py`: An example of using [os.system] instead of manually forking,
   execing, and waiting.

4. `ex_popen.py`: An example of using [os.popen] instead of manually creating a
   pipe, forking, and execing.

5. `ex_zombies.py`: A demonstration of what happens if parents don't [os.wait]
   for their children.

6. `ex_bomb.py`: A demonstration of what happens when [os.fork] is called too
   many times.

7. `ex_sigchld.py`: An example of using a signal handler to asynchronously
   handle waiting for a child.

8. `ex_sig_ign.py`: An example of simply ignore your children.

[os.system]:    https://docs.python.org/2/library/os.html#os.system
[os.popen]:     https://docs.python.org/2/library/os.html#os.popen
[os.fork]:      https://docs.python.org/2/library/os.html#os.fork
[os.wait]:      https://docs.python.org/2/library/os.html#os.wait
[processes]:    https://en.wikipedia.org/wiki/Process_(computing)
[signals]:      https://en.wikipedia.org/wiki/Unix_signal
