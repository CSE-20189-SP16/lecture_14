#!/usr/bin/env python2.7

# SIG_IGN example

import os
import signal
import sys
import time

def error(message, *args):
    print >>sys.stderr, message.format(*args)
    sys.exit(1)

N = int(sys.argv[1])

signal.signal(signal.SIGCHLD, signal.SIG_IGN)

for i in range(N):
    try:
        pid = os.fork()
        if pid == 0:    # Child
            print 'Child pid: {}, Parent pid: {}'.format(os.getpid(), os.getppid())
            os._exit(i)
    except OSError as e:# Error
        error('Unable to fork: {}', e)

for i in range(N):
    os.system('ps ux | grep "Z+" | grep -v grep')
    time.sleep(1)
