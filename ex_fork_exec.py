#!/usr/bin/env python2.7

# fork/exec example

import os
import sys

def error(message, *args):
    print >>sys.stderr, message.format(*args)
    sys.exit(1)

try:
    pid = os.fork()
except OSError as e:# Error
    error('Unable to fork: {}', e)

if pid == 0:    # Child
    print 'Child pid:  {}, Parent pid: {}'.format(os.getpid(), os.getppid())
    try:
        os.execlp('uname', 'uname', '-a') # Alternatively, os.execvp('uname', ['uname', '-a'])
    except OSError as e:
        error('Unable to exec: {}', e)
else:           # Parent
    print 'Parent pid: {}, Child pid:  {}'.format(os.getpid(), pid)
    pid, status = os.wait()
    print 'Child pid:  {}, Exit Status: {}'.format(pid, status)
