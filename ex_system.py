#!/usr/bin/env python2.7

# os.system example

import os
import sys

COMMAND = 'ls -l {}'.format(' '.join(sys.argv[1:]))

if os.system(COMMAND) == 0:
    print 'COMMAND {}: Success!'.format(COMMAND)
else:
    print 'COMMAND {}: Failure!'.format(COMMAND)
