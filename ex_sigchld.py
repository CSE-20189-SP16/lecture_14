#!/usr/bin/env python2.7

# SIGCHLD example

import os
import signal
import sys
import time

ChildPid    = -1
ChildStatus = -1

def sigchld_handler(signum, frame):
    global ChildPid, ChildStatus
    ChildPid, ChildStatus = os.wait()

try:
    pid = os.fork()
except OSError as e:# Error
    print >>sys.stderr, 'Unable to fork: {}'.format(e)
    sys.exit(1)

if pid == 0:    # Child
    print 'Child pid:  {}, Parent pid: {}'.format(os.getpid(), os.getppid())
    time.sleep(5)
else:           # Parent
    signal.signal(signal.SIGCHLD, sigchld_handler)

    while ChildPid < 0:
        print 'Parent pid: {}, Child pid:  {}'.format(os.getpid(), pid)
        time.sleep(1)

    print 'Child pid:  {}, Exit Status: {}'.format(ChildPid, ChildStatus)
