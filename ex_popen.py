#!/usr/bin/env python2.7

# os.popen example

import os
import sys

COMMAND = 'ls -l {}'.format(' '.join(sys.argv[1:]))

for index, line in enumerate(os.popen(COMMAND)):
    line = line.rstrip()
    print '{:08} {}'.format(index, line)
